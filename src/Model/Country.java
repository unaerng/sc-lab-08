package Model;

import Interface.Measurable;

public class Country implements Measurable {
	private String name;
	private double population;

	public Country(String name, double population) {
		this.name = name;
		this.population = population;
	}

	public String getName() {
		return this.name;
	}

	public double getPopulation() {
		return this.population;
	}

	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return this.getPopulation();
	}

}
